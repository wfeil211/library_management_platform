<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML>
<html>
  <head>
    <base href="<%=basePath%>">
    <title>导出</title>
<script type="text/javascript" src="<%=path %>/statics/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript" src="<%=path %>/statics/layer/layer.js"></script>
<link rel="stylesheet" type="text/css" href="<%=path %>/statics/bootstrap/css/bootstrap.min.css">
<script type="text/javascript" src="<%=path %>/statics/bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%=path %>/statics/plugins/bootstrap-table/bootstrap-table.min.css">
<script type="text/javascript" src="<%=path %>/statics/plugins/bootstrap-table/bootstrap-table.js"></script>
<script type="text/javascript" src="<%=path %>/statics/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%=path %>/statics/plugins/toastr/toastr.min.css">
<script type="text/javascript" src="<%=path %>/statics/plugins/toastr/toastr.min.js"></script>
<!-- 业务 配置 -->
<link rel="stylesheet" type="text/css" href="<%=path %>/statics/bootstrap/css/bootstrap.my.css">
<script type="text/javascript" src="<%=path %>/statics/js/order/exportExcel.js"></script>
</head>

<body>
	<div class="bootstrap-form">
		
		<form role="form" class="form-horizontal" id="exportExcel" method="post" enctype="multipart/form-data">
			<div class="text-center ">
			<label>你确定要导出所有订单信息吗？</label>
			</div>
			<div class="form-group form-group-button">
				<span class="col-xs-4"></span>
				<div class="col-xs-2">
					<button type="button" id="submitForm" class="btn btn-primary btn-sm">确认</button>
				</div>
				<div class="col-xs-2">
					<button type="button" onclick="exportExcel.closeLayer()" class="btn btn-primary btn-sm">取消</button>
				</div>
				<span class="col-xs-4"></span>
			</div>
		</form>
	</div>
	
	<script type="text/javascript">
		var exportExcel;
		$(function() {
			exportExcel = new ExportExcel();
			exportExcel.init();
		});
	</script>
	
</body>
</html>
