<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>首页 · 图书借阅平台--管理员</title>
    <link rel="stylesheet" href="<%=path %>/statics/frame/layui/css/layui.css">
    <link rel="stylesheet" href="<%=path %>/statics/css/style.css">
    <link rel="icon" href="<%=path %>/statics/image/code.png">
</head>
<body>

<!-- admin -->
<div class="layui-layout layui-layout-admin"> <!-- 添加skin-1类可手动修改主题为纯白，添加skin-2类可手动修改主题为蓝白 -->
    <!-- header -->
    <div class="layui-header my-header">
        <a href="index.jsp">
            <!--<img class="my-header-logo" src="" alt="logo">-->
            <div class="my-header-logo">图书借阅平台--管理员</div>
        </a>
        <div class="my-header-btn">
            <button class="layui-btn layui-btn-small btn-nav"><i class="layui-icon">&#xe620;</i></button>
        </div>
        <ul class="layui-nav my-header-user-nav" lay-filter="side-right">
            <li class="layui-nav-item">
                <a class="name" href="javascript:;"><i class="layui-icon">&#xe629;</i>主题</a>
                <dl class="layui-nav-child">
                    <dd data-skin="0"><a href="javascript:;">默认</a></dd>
                    <dd data-skin="1"><a href="javascript:;">纯白</a></dd>
                    <dd data-skin="2"><a href="javascript:;">蓝白</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item">
                <a class="name" href="javascript:;">${name} </a>
                <dl class="layui-nav-child">
                	<dd><a href="${pageContext.request.contextPath }/"><i class="layui-icon">&#x1006;</i>退出</a></dd>
                </dl>
            </li>
        </ul>
    </div>
    <!-- side -->
    <div class="layui-side my-side">
        <div class="layui-side-scroll">
            <ul class="layui-nav layui-nav-tree" lay-filter="side">

                <li class="layui-nav-item layui-nav-itemed"><!-- 去除 layui-nav-itemed 即可关闭展开 -->
                    <a href="javascript:;"><i class="layui-icon">&#xe622;</i>图书借阅</a>
                    <dl class="layui-nav-child">
                        <dd class="layui-nav-item"><a href="javascript:;" href-url="<%=path %>/PageController/requestPage.action?page=userManagement/userManagement"><i class="layui-icon">&#xe621;</i>用户管理</a></dd>
                        <dd class="layui-n	av-item"><a href="javascript:;" href-url="<%=path %>/PageController/requestPage.action?page=booksLobby/booksLobby"><i class="layui-icon">&#xe621;</i>图书大厅</a></dd>
                        <dd class="layui-nav-item"><a href="javascript:;" href-url="<%=path %>/PageController/requestPage.action?page=order/order"><i class="layui-icon">&#xe621;</i>我的订单</a></dd>
                    </dl>
                </li>
            </ul>
        </div>
    </div>
    <!-- body -->
    <div class="layui-body my-body">
        <div class="layui-tab layui-tab-card my-tab" lay-filter="card" lay-allowClose="true">
            <ul class="layui-tab-title">
                <li class="layui-this" lay-id="0"><span>欢迎页</span></li>
            </ul>
            <div class="layui-tab-content">
                <div class="layui-tab-item layui-show">
                    <iframe id="iframe" src="<%=path %>/PageController/requestPage.action?page=welcome/welcome" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>
    <!-- footer -->
    <div class="layui-footer my-footer">
        <p><a href="http://www.rongfeistudio.cn" target="_blank">仅供学习使用，请勿用作商业用途</a></p>
        <p>2017 © copyright 云算序</p>
    </div>
</div>

<script type="text/javascript" src="<%=path %>/statics/frame/layui/layui.js"></script>
<script type="text/javascript" src="<%=path %>/statics/js/index/index.js"></script>
</body>
</html>