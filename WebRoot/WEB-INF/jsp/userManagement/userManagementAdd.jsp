<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML >
<html>
<head>
<base href="<%=basePath%>">

<title>添加账号</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--bootstrap 配置-->
<script type="text/javascript" src="<%=path %>/statics/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript" src="<%=path %>/statics/jquery/jquery_form.js"></script>
<script type="text/javascript" src="<%=path %>/statics/plugins/layer/layer.js"></script>
<link rel="stylesheet" type="text/css" href="<%=path %>/statics/bootstrap/css/bootstrap.min.css">
<script type="text/javascript" src="<%=path %>/statics/bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%=path %>/statics/plugins/bootstrap-table/bootstrap-table.min.css">
<script type="text/javascript" src="<%=path %>/statics/plugins/bootstrap-table/bootstrap-table.js"></script>
<script type="text/javascript" src="<%=path %>/statics/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%=path %>/statics/plugins/toastr/toastr.min.css">
<script type="text/javascript" src="<%=path %>/statics/plugins/toastr/toastr.min.js"></script>

<link rel="stylesheet" type="text/css" href="<%=path %>/statics/plugins/bootstrapValidator/css/bootstrapValidator.min.css">
<script type="text/javascript" src="<%=path %>/statics/plugins/bootstrapValidator/js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="<%=path %>/statics/plugins/bootstrapValidator/js/bootstrapValidator.local.js"></script>
<script type="text/javascript" src="<%=path %>/statics/plugins/bootstrapValidator/js/language/zh_CN.js"></script>
<!-- 业务 配置 -->
<link rel="stylesheet" type="text/css" href="<%=path %>/statics/bootstrap/css/bootstrap.my.css">

<script type="text/javascript" src="<%=path %>/statics/js/userManagement/userManagementAdd.js"></script>

</head>
<body>
	<div class="bootstrap-form">
		<form role="form" class="form-horizontal" id="userManagementAdd" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label class="control-label col-xs-3 form-lable">账号：</label>
				<div class="col-xs-9">
					<input type="text" class="form-control" name="account" placeholder="请输入账号">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-3 form-lable">密码：</label>
				<div class="col-xs-9">
					<input type="text" class="form-control" name="password" placeholder="请输入密码">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-3 form-lable">身份：</label>
				<div class="col-xs-9">
					<select class="form-control" name="identity">
								<option value="">--请选择--</option>
								<option value="0">用户</option>
								<option value="1">管理员</option>
					</select>
				</div>
			</div>
			<div class="form-group form-group-button">
				<span class="col-xs-4"></span>
				<div class="col-xs-2">
					<button type="button" id="submitForm" class="btn btn-primary btn-sm">增加</button>
				</div>
				<span class="col-xs-1"></span>
				<div class="col-xs-2">
					<button type="button" onclick="userManagementAdd.closeLayer()" class="btn btn-primary btn-sm">取消</button>
				</div>
				<span class="col-xs-4"></span>
			</div>
		</form>
	</div>

	<script type="text/javascript">
		var userManagementAdd;
		$(function() {
			userManagementAdd = new UserManagementAdd();
			userManagementAdd.init();
		});
	</script>

</body>
</html>
