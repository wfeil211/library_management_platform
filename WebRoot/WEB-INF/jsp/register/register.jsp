<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>用户注册</title>
    <link rel="stylesheet" href="<%=path %>/statics/frame/layui/css/layui.css">
    <link rel="stylesheet" href="<%=path %>/statics/css/style.css">
    <link rel="icon" href="<%=path %>/statics/image/code.png">
</head>
<body>

<div class="login-main">
    <header class="layui-elip">用户注册</header>
    <form action="<%=path %>/UserController/register.action" class="layui-form" method="post">
        <div class="layui-input-inline">
            <input type="text" name="account" required  lay-verify="required" placeholder="账号" autocomplete="off" class="layui-input">
        </div>
        <div class="layui-input-inline">
            <input type="text" name="password" required  lay-verify="required" placeholder="密码" autocomplete="off" class="layui-input">
        </div>
        <div class="layui-input-inline">
           <label>**当前仅支持普通用户注册</label>
        </div>
           <label style="color: red;" >${register }</label>
        <div class="layui-input-inline login-btn">
            <button type="submit" class="layui-btn">注册</button>
        </div>
        <hr/>
        <p><a href="<%=path %>/login.jsp" class="fl">已有账号？立即登录</a></p>
    </form>
</div>


<script type="text/javascript" src="<%=path %>/statics/frame/layui/layui.js"></script>
<script type="text/javascript">
    layui.use(['form'], function () {
        var form = layui.form(), $ = layui.jquery;


    });
</script>
</body>
</html>