<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML >
<html>
<head>
<base href="<%=basePath%>">

<title>修改图书</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--bootstrap 配置-->
<script type="text/javascript" src="<%=path %>/statics/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript" src="<%=path %>/statics/jquery/jquery_form.js"></script>
<script type="text/javascript" src="<%=path %>/statics/plugins/layer/layer.js"></script>
<link rel="stylesheet" type="text/css" href="<%=path %>/statics/bootstrap/css/bootstrap.min.css">
<script type="text/javascript" src="<%=path %>/statics/bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%=path %>/statics/plugins/bootstrap-table/bootstrap-table.min.css">
<script type="text/javascript" src="<%=path %>/statics/plugins/bootstrap-table/bootstrap-table.js"></script>
<script type="text/javascript" src="<%=path %>/statics/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%=path %>/statics/plugins/toastr/toastr.min.css">
<script type="text/javascript" src="<%=path %>/statics/plugins/toastr/toastr.min.js"></script>

<link rel="stylesheet" type="text/css" href="<%=path %>/statics/plugins/bootstrapValidator/css/bootstrapValidator.min.css">
<script type="text/javascript" src="<%=path %>/statics/plugins/bootstrapValidator/js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="<%=path %>/statics/plugins/bootstrapValidator/js/bootstrapValidator.local.js"></script>
<script type="text/javascript" src="<%=path %>/statics/plugins/bootstrapValidator/js/language/zh_CN.js"></script>
<!-- 业务 配置 -->
<link rel="stylesheet" type="text/css" href="<%=path %>/statics/bootstrap/css/bootstrap.my.css">
<script type="text/javascript" charset="utf-8" src="<%=path %>/statics/plugins/utf8-jsp/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="<%=path %>/statics/plugins/utf8-jsp/ueditor.all.min.js"> </script>
<!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
<!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
<script type="text/javascript" charset="utf-8" src="<%=path %>/statics/plugins/utf8-jsp/lang/zh-cn/zh-cn.js"></script>
<script type="text/javascript" charset="utf-8" src="<%=path %>/statics/bootstrap-fileinput/js/fileinput.min.js"></script>
<script type="text/javascript" charset="utf-8" src="<%=path %>/statics/bootstrap-fileinput/js/zh.js"></script>
<link rel="stylesheet" type="text/css" href="<%=path %>/statics/bootstrap-fileinput/css/fileinput.min.css">

<script type="text/javascript" src="<%=path %>/statics/js/booksLobby/booksLobbyEdit.js"></script>

</head>
<body>
	<div class="bootstrap-form">
		<form role="form" class="form-horizontal" id="booksLobbyEdit" method="post" enctype="multipart/form-data">
			<input type="hidden" class="form-control" name="id">
			<div class="form-group">
				<label class="control-label col-xs-3 form-lable">书名：</label>
				<div class="col-xs-9">
					<input type="text" class="form-control" name="name" placeholder="请输入书名">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-3 form-lable">ISBN：</label>
				<div class="col-xs-9">
					<input type="text" class="form-control" name="isbn" placeholder="请输入图书编号">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-3 form-lable">分类：</label>
				<div class="col-xs-9">
					<select class="form-control" name="classify">
						<option value="">--请选择--</option>
						<option value="历史">历史</option>
						<option value="科技">科技</option>
						<option value="少儿">少儿</option>
						<option value="军事">军事</option>
						<option value="动漫">动漫</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-3 form-lable">作者：</label>
				<div class="col-xs-9">
					<input type="text" class="form-control" name="author" placeholder="请输入作者">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-3 form-lable">数量：</label>
				<div class="col-xs-9">
					<input type="text" class="form-control" name="num" placeholder="请输入图书数量">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-3 form-lable">封面(不选则不修改)：</label>
				<div class="col-xs-9">
					<input type="file" class="form-control" id="imageFile" name="imageFile" placeholder="请输入图书封面">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-3 form-lable">多媒体上传(测试阶段)：</label>
				<div class="col-xs-9">
					<input id="reportFile" name="reportFile" type="file" class="file-loading" placeholder="请输入图书封面">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-3 form-lable">视频上传(测试阶段)：</label>
				<div class="col-xs-9">
					<input id="videoFile" name="videoFile" type="file" class="file-loading" placeholder="请输入图书封面">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-xs-3 form-lable">详细说明：</label>
				<div class="col-xs-9">
					<textarea id="editor" rows="5" cols="5" name="info"></textarea>
				</div>
			</div>
			<div class="form-group form-group-button">
				<span class="col-xs-10"></span>
				<div class="col-xs-1">
					<button type="button" id="submitForm" class="btn btn-primary btn-sm">修改</button>
				</div>
				<div class="col-xs-1">
					<button type="button" onclick="booksLobbyEdit.closeLayer()" class="btn btn-primary btn-sm">取消</button>
				</div>
			</div>
		</form>
	</div>

	<script type="text/javascript">
		var booksLobbyAdd;
		var ue = UE.getEditor('editor');
		var info=sessionStorage.getItem("info");	
		ue.ready(function() {//编辑器初始化完成再赋值
			ue.setContent(info);  //赋值给UEditor
		});
		var pathName = window.document.location.pathname;
		var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
		$(function() {
			booksLobbyEdit = new BooksLobbyEdit();
			booksLobbyEdit.init();			
			//初始化fileinput控件（第一次初始化）
            initFileInput("reportFile", projectName+"/BooksController/upload.action");	
            videoFileInput("videoFile", projectName+"/BooksController/videoUpload.action");
		});
		//初始化fileinput控件（第一次初始化）
		function initFileInput(ctrlName, uploadUrl) {    
		    var control = $('#' + ctrlName); 

		    control.fileinput({
		    	language: 'zh',                                         //设置语言
	            uploadUrl: uploadUrl,                                   //上传的地址
	            allowedFileExtensions: ['jpg', 'png','mp4','zip'],    //接收的文件后缀
	            showBrowse: true,										//是否显示浏览按钮
	            showCancel: true,										//是否显示取消按钮
	            showUpload: true,                                      //是否显示上传按钮
	            showRemove : true,                                     //显示移除按钮
	            showPreview : true,                                     //是否显示预览
	            showCaption: false,                                     //是否显示标题
	            browseClass: "btn btn-primary",                         //按钮样式
	            dropZoneEnabled: false,									//是否显示拖拽区域
	            uploadAsync: true,
	            //minImageWidth: 50,                                    //图片的最小宽度
	            //minImageHeight: 50,                                   //图片的最小高度
	            //maxImageWidth: 1000,                                  //图片的最大宽度
	            //maxImageHeight: 1000,                                 //图片的最大高度
	            maxFileSize: 500000000,                                       //单位为kb，如果为0表示不限制文件大小
	            minFileCount: 0,
	            maxFileCount: 1,										//表示允许同时上传的最大文件个数
	            enctype: 'multipart/form-data',
	            validateInitialCount: true,
	            previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
	            msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",
	        	// 这个配置就是解决办法了,初始化时限制图片大小
	            previewSettings: {
	                image: {width: "100px", height: "100px"},
	            }
		    });
		  	//导入文件上传完成之后的事件
	        control.on('fileuploaded', function(event, data, previewId, index){
	            var response = data.response;
	            $.each(response,function(id,path){//上传完毕，将文件名返回
	                console.log("path:"+path.pathUrl);
	                $("#booksLobbyEdit").append("<input class='imgClass' name='multimedia' type='hidden' value='"+path.pathUrl+"'>");
	            });
	 
	        });
		}
		function videoFileInput(ctrlName, uploadUrl) {    
		    var control = $('#' + ctrlName); 

		    control.fileinput({
		    	language: 'zh',                                         //设置语言
	            uploadUrl: uploadUrl,                                   //上传的地址
	            allowedFileExtensions: ['mp4'],    //接收的文件后缀
	            showBrowse: true,										//是否显示浏览按钮
	            showCancel: true,										//是否显示取消按钮
	            showUpload: true,                                      //是否显示上传按钮
	            showRemove : true,                                     //显示移除按钮
	            showPreview : true,                                     //是否显示预览
	            showCaption: false,                                     //是否显示标题
	            browseClass: "btn btn-primary",                         //按钮样式
	            dropZoneEnabled: false,									//是否显示拖拽区域
	            uploadAsync: true,
	            //minImageWidth: 50,                                    //图片的最小宽度
	            //minImageHeight: 50,                                   //图片的最小高度
	            //maxImageWidth: 1000,                                  //图片的最大宽度
	            //maxImageHeight: 1000,                                 //图片的最大高度
	            maxFileSize: 500000000,                                       //单位为kb，如果为0表示不限制文件大小
	            minFileCount: 0,
	            maxFileCount: 1,										//表示允许同时上传的最大文件个数
	            enctype: 'multipart/form-data',
	            validateInitialCount: true,
	            previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
	            msgFilesTooMany: "选择上传的文件数量({n}) 超过允许的最大数值{m}！",
	        	// 这个配置就是解决办法了,初始化时限制图片大小
	            previewSettings: {
	                image: {width: "100px", height: "100px"},
	            }
		    });
		  	//导入文件上传完成之后的事件
	        control.on('fileuploaded', function(event, data, previewId, index){
	            var response = data.response;
	            $.each(response,function(id,path){//上传完毕，将文件名返回
	                console.log("path:"+path.pathUrl);
	                $("#booksLobbyEdit").append("<input class='imgClass' name='video' type='hidden' value='"+path.pathUrl+"'>");
	            });
	 
	        });
		}
	</script>

</body>
</html>
