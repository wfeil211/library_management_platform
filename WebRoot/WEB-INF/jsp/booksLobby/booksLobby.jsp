<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML>
<html>
  <head>
    <base href="<%=basePath%>">
    <title>图书大厅</title>
<script type="text/javascript" src="<%=path %>/statics/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript" src="<%=path %>/statics/layer/layer.js"></script>
<link rel="stylesheet" type="text/css" href="<%=path %>/statics/bootstrap/css/bootstrap.min.css">
<script type="text/javascript" src="<%=path %>/statics/bootstrap/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%=path %>/statics/plugins/bootstrap-table/bootstrap-table.min.css">
<script type="text/javascript" src="<%=path %>/statics/plugins/bootstrap-table/bootstrap-table.js"></script>
<script type="text/javascript" src="<%=path %>/statics/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%=path %>/statics/plugins/toastr/toastr.min.css">
<script type="text/javascript" src="<%=path %>/statics/plugins/toastr/toastr.min.js"></script>
<!-- 业务 配置 -->
<link rel="stylesheet" type="text/css" href="<%=path %>/statics/bootstrap/css/bootstrap.my.css">
<script type="text/javascript" src="<%=path %>/statics/js/booksLobby/booksLobby.js"></script>
</head>
<body>
	<div class="panel-body">
		<div id="search_form" class="panel panel-default">
			<div class="panel-heading">查询条件</div>
			<div class="panel-body">
				<form id="formSearch" class="query-form">
						<label class="control-label col-sm-1">书名：</label>
						<div class="col-sm-2">
							<input type="text" class="form-control" id="search_name">
						</div>
						<label class="control-label col-sm-1">分类</label>
						<div class="col-sm-2">
							<select class="form-control" id="search_classify">
								<option value="">--请选择--</option>
								<option value="历史">历史</option>
								<option value="科技">科技</option>
								<option value="少儿">少儿</option>
								<option value="军事">军事</option>
								<option value="动漫">动漫</option>
							</select>
						</div>
						<div class="col-sm-1" style="text-align:left;">
							<button type="button" id="btn_query" class="btn btn-primary btn-sm">查询</button>
						</div>
				</form>
			</div>
		</div>
		<div id="toolbar"></div>
		<table id="tb_booksLobby"></table>
	</div>
	<script type="text/javascript">
		$(function() {
			var booksLobby = new BooksLobby();
			booksLobby.init();
		});		
		/**
		 * 设置form值
		 */
		function setFormValuesByMap(fm, obj) {
			if (fm && obj) {
				for (var key in obj) {
					if (fm[key] && obj[key] != null) {
						fm[key].value = obj[key];
					}
				}
			}
		}
	</script>
</body>
</html>