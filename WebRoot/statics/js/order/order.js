function Order(config) {
	var t = this;
	var pathName = window.document.location.pathname;
	var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
	//var projectName = "";
	var identity=document.cookie.split("=")[1];
	// 得到查询的参数
	this.toolbarId = "toolbar";
	this.tableId = "tb_order";
	this.queryParams = function(params) {
		var temp = { // 这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
			limit : params.limit, // 页面大小
			start : params.offset, // 页码
			orderno : $("#search_orderno").val()
		};
		return temp;
	};
	// 初始化Table
	this.tableBind = function() {
		$('#' + t.tableId).bootstrapTable({
			url : 'OrderController/getGridStore.action', // 请求后台的URL（*）
			method : 'post', // 请求方式（*）
			toolbar : '#' + t.toolbarId, // 工具按钮用哪个容器
			striped : true, // 是否显示行间隔色
			cache : false, // 是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
			pagination : true, // 是否显示分页（*）
			// sortable : false, // 是否启用排序
			sortOrder : "asc", // 排序方式
			queryParams : t.queryParams,// 传递参数（*）
			sidePagination : "server", // 分页方式：client客户端分页，server服务端分页（*）
			pageNumber : 1, // 初始化加载第一页，默认第一页
			pageSize : 10, // 每页的记录行数（*）
			pageList : [10, 25, 50, 100], // 可供选择的每页的行数（*）
			search : false, // 是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
			strictSearch : true,
			showColumns : true, // 是否显示所有的列
			showRefresh : true, // 是否显示刷新按钮
			minimumCountColumns : 2, // 最少允许的列数
			totalField : 'totalProperty',
			dataField : 'list',
			clickToSelect : true, // 是否启用点击选中行
			height : $(document).height() - $("#search_form").height()
					- (2 + 5), // 行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
			uniqueId : "id", // 每一行的唯一标识，一般为主键列
			showToggle : true, // 是否显示详细视图和列表视图的切换按钮
			cardView : false, // 是否显示详细视图
			detailView : false, // 是否显示父子表
			columns : [{
						checkbox : true
					},{
						field : 'orderno',
						width : 80,
						title : '订单号'
					},{
						field : 'isbn',
						width : 80,
						title : '图书编号'
					},{
						field : 'name',
						width : 80,
						title : '姓名'
					},{
						field : 'phone',
						width : 80,
						title : '联系方式'
					},{
						field : 'createtime',
						width : 100,
						title : '借阅时间'
					},{
						field : 'borstuts',
						title : '借阅状态 ',
						width : 150,
						formatter : function(value, row, index) {
							if (value == 0) {
								return '<font color="red">待借出</font>';
							} else if (value == 1) {
								return '<font color="blue">已借出</font>';
							} else if (value == 2) {
								return '<font color="green">已归还</font>';
							}
							return value;
						}
					},{
						field : 'num',
						width : 80,
						title : '借阅数量'
					},{
						field : 'gobacktime',
						width : 100,
						title : '归还时间'
					}]
		});
	};
	//借出
	this.btnOutBind = function(id) {
		if(identity==1){
			$("#" + t.toolbarId)
					.append(' <button id="'
							+ id
							+ '" type="button" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-resize-full" aria-hidden="true"></span>借出</button>');

			$("#" + id).click(function() {
				var arrselections = $("#" + t.tableId)
						.bootstrapTable('getSelections');
				if (arrselections.length == 0) {
					top.layer.msg('请选择有效数据');
					return;
				}
				for(var i=0;i<arrselections.length;i++){
					if(arrselections[i].borstuts!='0'){
						top.layer.msg('此数据包含已借出或已归还订单，请选择其他选项');
						return;
					}
				}				
				top.layer.confirm('确认要借出选择的数据吗？', {
							title : '温馨提示',
							btn : ['确认', '取消']
						}, function(index) {
							// 确认
							var ids = "";
							for (var i = 0; i < arrselections.length; i++) {
								if (i != 0) {
									ids += ",";
								}
								ids += arrselections[i]['id'];
							}
							$.post("OrderController/outBooks.action", {
										ids : ids
									}, function(data) {
										if (data.execute) {
											top.layer.msg(data.msg);
										} else {
											top.layer.msg(data.msg);
										}
										$("#" + t.tableId).bootstrapTable('refresh');
										top.layer.close(index);
									}, "json");

						}, function() {
							$("#" + t.tableId).bootstrapTable('refresh');
						});
			});
		}
	}
	//归还
	this.btnIntBind = function(id) {
		if(identity==1){
			$("#" + t.toolbarId)
					.append(' <button id="'
							+ id
							+ '" type="button" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon glyphicon-resize-small" aria-hidden="true"></span>归还</button>');

			$("#" + id).click(function() {
				var arrselections = $("#" + t.tableId)
						.bootstrapTable('getSelections');
				if (arrselections.length == 0) {
					top.layer.msg('请选择有效数据');
					return;
				}
				for(var i=0;i<arrselections.length;i++){
					if(arrselections[i].borstuts!='1'){
						top.layer.msg('此数据包含待借出或已归还订单，请选择其他选项');
						return;
					}
				}	
				top.layer.confirm('确认要归还选择的数据吗？', {
							title : '温馨提示',
							btn : ['确认', '取消']
						}, function(index) {
							// 确认
							var ids = "";
							for (var i = 0; i < arrselections.length; i++) {
								if (i != 0) {
									ids += ",";
								}
								ids += arrselections[i]['id'];
							}
							$.post("OrderController/inBooks.action", {
										ids : ids
									}, function(data) {
										if (data.execute) {
											top.layer.msg(data.msg);
										} else {
											top.layer.msg(data.msg);
										}
										$("#" + t.tableId).bootstrapTable('refresh');
										top.layer.close(index);
									}, "json");

						}, function() {
							$("#" + t.tableId).bootstrapTable('refresh');
						});
			});
		}
	}
	//导出
	this.btnExport = function(id) {
		if(identity==1){
			$("#" + t.toolbarId)
					.append(' <button id="'
							+ id
							+ '" type="button" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-export" aria-hidden="true"></span>导出</button>');
			$("#" + id).click(function() {
				
				top.layer.open({
					type : 2,
					shade : 0.5,
					title : '导出 ',
					area : ['500px',
							'200px'],
					maxmin : false,
					content : projectName+'/PageController/requestPage.action?page=order/exportExcel',
					zIndex : layer.zIndex, // 重点1
					success : function(layero, index) {
						var body = top.layer.getChildFrame('body', index);
					},
					end : function() {
						$("#" + t.tableId).bootstrapTable('refresh');
					}
				});
			});
		}
	}
	// button事件绑定
	this.buttonBind = function() {
		// 借出
		t.btnOutBind("btn_out");
		// 归还
		t.btnIntBind("btn_int");
		// 导出
		t.btnExport("btn_exp");
		// 查询
		$("#btn_query").click(function() {
					$("#" + t.tableId).bootstrapTable('refresh', {
								pageNumber : 1
							});
				});

	}
	this.init = function() {
		t.tableBind();
		t.buttonBind();
	}
}
