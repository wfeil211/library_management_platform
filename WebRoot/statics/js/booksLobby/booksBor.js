function BooksBor(config) {
	var t = this;

	// form验证初始化
	this.formValidatorInit = function() {
		$("#booksBor").bootstrapValidator({
					message : 'This value is not valid',
					feedbackIcons : {
						valid : 'glyphicon glyphicon-ok',
						invalid : 'glyphicon glyphicon-remove',
						validating : 'glyphicon glyphicon-refresh'
					},
					fields: {							                    
		                name: {
		                    validators: {
		                        notEmpty: {
		                            message: '姓名不能为空'
		                        }
		                    }
		                 },
		                 isbn: {
			                    validators: {
			                        notEmpty: {
			                            message: 'ISBN不能为空'
			                        },
			                        regexp: {
			                            regexp: /^\d{13}$/,
			                            message: 'ISBN必须为13位正整数'
			                        }
			                    }
			               },
			               num: {
			                    validators: {
			                        notEmpty: {
			                            message: '借阅数量不能为空'
			                        }
			                    }
			               },
			               phone: {
			                    validators: {
			                        notEmpty: {
			                            message: '手机号不能为空'
			                        }
			                    }
			               },
			               gobacktime: {
			                    validators: {
			                        notEmpty: {
			                            message: '归还时间不能为空'
			                        },
			                        callback: {
			                            message: '归还时间必须是将来时',
			                            callback: function (value, validator, $field) {
			                                var start = new Date();
			                                var end = new Date(value.replace("-", "/").replace("-", "/"));
			     
			                                if (start <= end) {
			                                    return true;
			                                }
			                                return false;
			                            }
			                        }
			                    }
			               }
		                
					}
				});
	}

	// 提交数据
	this.submitForm = function() {
		$("#booksBor").bootstrapValidator("validate"); // 验证
		
		if ($("#booksBor").data("bootstrapValidator").isValid()) {
			$("#booksBor").ajaxSubmit({
				url : "OrderController/addOrder.action",
				type : "post",
				dataType : "json",
				success : function(data) {
					if (data.execute) {
						top.layer.msg(data.msg);
						parent.layer.closeAll('iframe');
					} else {
						if (data.data) {
							var fieldName = data.data;
							$("#booksBor").bootstrapValidator("addField",
									fieldName, {
										validators : {
											customValid : {
												message : data.msg,
												oldValue : $("#booksBor")[0][fieldName].value
											}
										}
									});
							$("#booksBor").data("bootstrapValidator")
									.validateField(fieldName);
						} else {
							top.layer.msg(data.msg);
						}
					}					
				}
			});
		}
	}

	
	//取消
	this.closeLayer = function() {
		parent.layer.closeAll('iframe');
	};

	this.buttonBind = function() {
		$("#submitForm").click(function() {
					t.submitForm();
				});
	};

	// 初始化
	this.init = function() {
		t.formValidatorInit();
		t.buttonBind();
	}
}