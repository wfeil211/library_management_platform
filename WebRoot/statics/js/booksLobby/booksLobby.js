var pathName = window.document.location.pathname;
var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
//var projectName = "";
function BooksLobby(config) {
	var t = this;	
	var identity=document.cookie.split("=")[1];
	// 得到查询的参数
	this.toolbarId = "toolbar";
	this.tableId = "tb_booksLobby";
	this.queryParams = function(params) {
		var temp = { // 这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
			limit : params.limit, // 页面大小
			start : params.offset, // 页码
			name : $("#search_name").val(),
			classify : $("#search_classify").val()
		};
		return temp;
	};
	// 初始化Table
	this.tableBind = function() {
		$('#' + t.tableId).bootstrapTable({
			url : 'BooksController/getGridStore.action', // 请求后台的URL（*）
			method : 'post', // 请求方式（*）
			toolbar : '#' + t.toolbarId, // 工具按钮用哪个容器
			striped : true, // 是否显示行间隔色
			cache : false, // 是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
			pagination : true, // 是否显示分页（*）
			// sortable : false, // 是否启用排序
			sortOrder : "asc", // 排序方式
			queryParams : t.queryParams,// 传递参数（*）
			sidePagination : "server", // 分页方式：client客户端分页，server服务端分页（*）
			pageNumber : 1, // 初始化加载第一页，默认第一页
			pageSize : 10, // 每页的记录行数（*）
			pageList : [10, 25, 50, 100], // 可供选择的每页的行数（*）
			search : false, // 是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
			strictSearch : true,
			showColumns : true, // 是否显示所有的列
			showRefresh : true, // 是否显示刷新按钮
			minimumCountColumns : 2, // 最少允许的列数
			totalField : 'totalProperty',
			dataField : 'list',
			clickToSelect : true, // 是否启用点击选中行
			height : $(document).height() - $("#search_form").height()
					- (2 + 5), // 行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
			uniqueId : "id", // 每一行的唯一标识，一般为主键列
			showToggle : true, // 是否显示详细视图和列表视图的切换按钮
			cardView : false, // 是否显示详细视图
			detailView : false, // 是否显示父子表
			columns : [{
						checkbox : true
					},{
						field : 'name',
						width : 80,
						title : '名称'
					},{
						field : 'image',
						title : '封面',
						width : 105,
						formatter : function(value, row, index) {

							return '<img src="' +  value
									+ '" height="50" width="100" />';
						}
					},{
						field : 'classify',
						width : 80,
						title : '分类'
					},{
						field : 'author',
						width : 80,
						title : '作者'
					},{
						field : 'isbn',
						width : 80,
						title : '图书编号'
					},{
						field : 'num',
						width : 80,
						title : '数量'
					},{
						field : 'multimedia',
						title : '多媒体',
						width : 105,
						formatter : function(value, row, index) {

							return '<a href="' +  value
									+ '" download >点此下载</>';
						}
					},{
						field : 'video',
						title : '视频',
						width : 105,
						formatter : function(value, row, index) {
							return '<input onclick="watch(\''+row.image + ',' +row.video+'\')" type="button" class="btn btn-success radius size-L" value="查看" />';
						}
					},{
						field : 'info',
						width : 80,
						title : '详细信息'
					},{
						field : 'createtime',
						width : 100,
						title : '创建时间'
					}]
		});
	};
	//添加
	this.btnAddBind = function(id) {
		if(identity==1){
			$("#" + t.toolbarId)
			.append(
					' <button id="'
							+ id
							+ '" type="button" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>增加</button>');
			$("#" + id).click(function() {
				top.layer.open({
					type : 2,
					shade : 0.5,
					title : '添加图书',
					area : ['1000px',
							'750px'],
					maxmin : false,
					content : projectName+'/PageController/requestPage.action?page=booksLobby/booksLobbyAdd',
					zIndex : layer.zIndex, // 重点1
					success : function(layero, index) {
						var body = top.layer.getChildFrame('body', index);
					},
					end : function() {
						$("#" + t.tableId).bootstrapTable('refresh');
					}
				});
		
			});
		}			
	}
	//删除
	this.btnDelBind = function(id) {
		if(identity==1){
			$("#" + t.toolbarId)
					.append(' <button id="'
							+ id
							+ '" type="button" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>删除</button>');

			$("#" + id).click(function() {
				var arrselections = $("#" + t.tableId)
						.bootstrapTable('getSelections');
				if (arrselections.length == 0) {
					top.layer.msg('请选择有效数据');
					return;
				}
				top.layer.confirm('确认要删除选择的数据吗？', {
							title : '温馨提示',
							btn : ['确认', '取消']
						}, function(index) {
							// 确认
							var ids = "";
							for (var i = 0; i < arrselections.length; i++) {
								if (i != 0) {
									ids += ",";
								}
								ids += arrselections[i]['id'];
							}
							$.post("BooksController/delBook.action", {
										ids : ids
									}, function(data) {
										if (data.execute) {
											top.layer.msg(data.msg);
										} else {
											top.layer.msg(data.msg);
										}
										$("#" + t.tableId).bootstrapTable('refresh');
										top.layer.close(index);
									}, "json");

						}, function() {
							$("#" + t.tableId).bootstrapTable('refresh');
						});
			});
		}
	}
	//借阅
	this.btnBorBind = function(id) {
			$("#" + t.toolbarId)
					.append(
							' <button id="'
									+ id
									+ '" type="button" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-send" aria-hidden="true"></span>借阅</button>');
			$("#" + id).click(function() {
				var arrselections = $("#" + t.tableId)
						.bootstrapTable('getSelections');
				if (arrselections.length == 0) {
					top.layer.msg('请选择借阅图书');
					return;
				}else if(arrselections.length > 1){
					top.layer.msg('一次只能借阅1类书');
					return;
				}
				//借阅的图书编号集合
					top.layer.open({
						type : 2,
						shade : 0.5,
						title : '借阅图书',
						area : ['500px',
								'400px'],
						maxmin : false,
						content : projectName+'/PageController/requestPage.action?page=booksLobby/booksBor',
						zIndex : layer.zIndex, // 重点1
						success : function(layero, index) {
							var body = top.layer.getChildFrame('body', index);
							$("#isbn", body).val(arrselections[0].isbn);
						},
						end : function() {
							$("#" + t.tableId).bootstrapTable('refresh');
						}
					});
			});			
	}
	//修改
	this.btnEdit = function(id) {
		if(identity==1){
			$("#" + t.toolbarId)
			.append(
					' <button id="'
							+ id
							+ '" type="button" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>修改</button>');						
			$("#" + id).click(function() {
				var arrselections = $("#" + t.tableId)
							.bootstrapTable('getSelections');
				if (arrselections.length == 0) {
					top.layer.msg('请选择图书');
					return;
				}else if(arrselections.length > 1){
					top.layer.msg('一次只可修改一条');
					return;
				}
				sessionStorage.setItem("info", arrselections[0].info);
				top.layer.open({
					type : 2,
					shade : 0.5,
					title : '修改图书',
					area : ['1000px',
							'750px'],
					maxmin : false,
					content : projectName+'/PageController/requestPage.action?page=booksLobby/booksLobbyEdit',
					zIndex : layer.zIndex, // 重点1
					success : function(layero, index) {
						var body = top.layer.getChildFrame('body', index);
						// 赋值
						setFormValuesByMap($("#booksLobbyEdit", body)[0], arrselections[0]);
					},
					end : function() {
						$("#" + t.tableId).bootstrapTable('refresh');
					}
				});
		
			});
		}			
	}
	// button事件绑定
	this.buttonBind = function() {
		// 增加
		t.btnAddBind("btn_add");
		// 修改
		t.btnEdit("btn_edit");
		// 删除
		t.btnDelBind("btn_del");
		// 借阅
		t.btnBorBind("btn_bor");
		// 查询
		$("#btn_query").click(function() {
					$("#" + t.tableId).bootstrapTable('refresh', {
								pageNumber : 1
							});
				});

	}
	this.init = function() {
		t.tableBind();
		t.buttonBind();
	}
}
function watch(path){
	var path=path.split(",");
	var cover=path[0];
	var video=path[1];
		top.layer
		.open({
			type : 2,
			shade : 0.5,
			title : '预览上传视频 ',
			area : [600+'px',500+'px'],
			maxmin : false,
			content : projectName+'/statics/ckplayer/flashplayer.html?video='+video+'&cover='+cover,
			zIndex : layer.zIndex, // 重点1
			success : function(layero, index) {
			}
		
		});
	}
