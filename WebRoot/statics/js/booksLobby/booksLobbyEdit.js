function BooksLobbyEdit(config) {
	var t = this;
	// form验证初始化
	this.formValidatorInit = function() {
		$("#booksLobbyEdit").bootstrapValidator({
					message : 'This value is not valid',
					feedbackIcons : {
						valid : 'glyphicon glyphicon-ok',
						invalid : 'glyphicon glyphicon-remove',
						validating : 'glyphicon glyphicon-refresh'
					},
					fields: {							                    
		                name: {
		                    validators: {
		                        notEmpty: {
		                            message: '书名不能为空'
		                        }
		                    }
		                 },
		                 isbn: {
			                    validators: {
			                        notEmpty: {
			                            message: 'ISBN不能为空'
			                        },
			                        regexp: {
			                            regexp: /^\d{13}$/,
			                            message: 'ISBN必须为13位正整数'
			                        }
			                    }
			               },
			               num: {
			                    validators: {
			                        notEmpty: {
			                            message: '数量不能为空'
			                        },
			                        digits : {
			                            message : '字段必须是正整数'
			                        }
			                    }
			               },
			               info: {
			                    validators: {
			                        notEmpty: {
			                            message: '详细说明不能为空'
			                        }
			                    }
			               }
		                
					}
				});
	}

	// 提交数据
	this.submitForm = function() {
		$("#booksLobbyEdit").bootstrapValidator("validate"); // 验证
		//文件解析
		var imageFile = $('#imageFile').val();
		if (imageFile == '') {
			
		} else {
			name = imageFile.substring(imageFile.length - 4, imageFile.length);
			if (name == '.jpg' || name == 'jpeg' || name == '.png') {

			} else {
				top.layer.msg('封面请上传jpg或者jpeg或者png文件');
				return;
			}
		}
		
		if ($("#booksLobbyEdit").data("bootstrapValidator").isValid()) {
			$("#booksLobbyEdit").ajaxSubmit({
				url : "BooksController/editBook.action",
				type : "post",
				dataType : "json",
				success : function(data) {
					if (data.execute) {
						top.layer.msg(data.msg);
						parent.layer.closeAll('iframe');
					} else {
						if (data.data) {
							var fieldName = data.data;
							$("#booksLobbyEdit").bootstrapValidator("addField",
									fieldName, {
										validators : {
											customValid : {
												message : data.msg,
												oldValue : $("#booksLobbyEdit")[0][fieldName].value
											}
										}
									});
							$("#booksLobbyEdit").data("bootstrapValidator")
									.validateField(fieldName);
						} else {
							top.layer.msg(data.msg);
						}
					}					
				}
			});
		}
	}

	
	//取消
	this.closeLayer = function() {
		parent.layer.closeAll('iframe');
	};

	this.buttonBind = function() {
		$("#submitForm").click(function() {
					t.submitForm();
				});
	};

	// 初始化
	this.init = function() {
		t.formValidatorInit();
		t.buttonBind();
	}
}