function UserManagementAdd(config) {
	var t = this;

	// form验证初始化
	this.formValidatorInit = function() {
		$("#userManagementAdd").bootstrapValidator({
					message : 'This value is not valid',
					feedbackIcons : {
						valid : 'glyphicon glyphicon-ok',
						invalid : 'glyphicon glyphicon-remove',
						validating : 'glyphicon glyphicon-refresh'
					},
					fields: {
						identity: {
		                    validators: {
		                        notEmpty: {
		                            message: '身份不能为空'
		                        }
		                    }
						},	                    
		                account: {
		                    validators: {
		                        notEmpty: {
		                            message: '账号不能为空'
		                        }
		                    }
		                },
		                password: {
		                    validators: {
		                        notEmpty: {
		                            message: '密码不能为空'
		                        }
		                    }
		                }
		                
					}
				});
	}

	// 提交数据
	this.submitForm = function() {
		$("#userManagementAdd").bootstrapValidator("validate"); // 验证

		if ($("#userManagementAdd").data("bootstrapValidator").isValid()) {
			$("#userManagementAdd").ajaxSubmit({
				url : "UserController/addUser.action",
				type : "post",
				dataType : "json",
				success : function(data) {
					if (data.execute) {
						top.layer.msg(data.msg);
						parent.layer.closeAll('iframe');
					} else {
						if (data.data) {
							var fieldName = data.data;
							$("#userManagementAdd").bootstrapValidator("addField",
									fieldName, {
										validators : {
											customValid : {
												message : data.msg,
												oldValue : $("#userManagementAdd")[0][fieldName].value
											}
										}
									});
							$("#userManagementAdd").data("bootstrapValidator")
									.validateField(fieldName);
						} else {
							top.layer.msg(data.msg);
						}
					}					
				}
			});
		}
	}

	
	//取消
	this.closeLayer = function() {
		parent.layer.closeAll('iframe');
	};

	this.buttonBind = function() {
		$("#submitForm").click(function() {
					t.submitForm();
				});
	};

	// 初始化
	this.init = function() {
		t.formValidatorInit();
		t.buttonBind();
	}
}