function UrderOrder(config) {
	var t = this;
	var pathName = window.document.location.pathname;
	var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
	//var projectName = "";
	var identity=document.cookie.split("=")[1];
	// 得到查询的参数
	this.toolbarId = "toolbar";
	this.tableId = "tb_urderOrder";
	this.queryParams = function(params) {
		var temp = { // 这里的键的名字和控制器的变量名必须一直，这边改动，控制器也需要改成一样的
			limit : params.limit, // 页面大小
			start : params.offset, // 页码
			adminid : $("#search_adminid").val()
		};
		return temp;
	};
	// 初始化Table
	this.tableBind = function() {
		$('#' + t.tableId).bootstrapTable({
			url : 'OrderController/getGridStore.action', // 请求后台的URL（*）
			method : 'post', // 请求方式（*）
			toolbar : '#' + t.toolbarId, // 工具按钮用哪个容器
			striped : true, // 是否显示行间隔色
			cache : false, // 是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
			pagination : true, // 是否显示分页（*）
			// sortable : false, // 是否启用排序
			sortOrder : "asc", // 排序方式
			queryParams : t.queryParams,// 传递参数（*）
			sidePagination : "server", // 分页方式：client客户端分页，server服务端分页（*）
			pageNumber : 1, // 初始化加载第一页，默认第一页
			pageSize : 10, // 每页的记录行数（*）
			pageList : [10, 25, 50, 100], // 可供选择的每页的行数（*）
			search : false, // 是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
			strictSearch : true,
			showColumns : true, // 是否显示所有的列
			showRefresh : true, // 是否显示刷新按钮
			minimumCountColumns : 2, // 最少允许的列数
			totalField : 'totalProperty',
			dataField : 'list',
			clickToSelect : true, // 是否启用点击选中行
			height : $(document).height() - $("#search_form").height()
					- (2 + 5), // 行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
			uniqueId : "id", // 每一行的唯一标识，一般为主键列
			showToggle : true, // 是否显示详细视图和列表视图的切换按钮
			cardView : false, // 是否显示详细视图
			detailView : false, // 是否显示父子表
			columns : [{
						checkbox : true
					},{
						field : 'orderno',
						width : 80,
						title : '订单号'
					},{
						field : 'isbn',
						width : 80,
						title : '图书编号'
					},{
						field : 'name',
						width : 80,
						title : '姓名'
					},{
						field : 'phone',
						width : 80,
						title : '联系方式'
					},{
						field : 'createtime',
						width : 100,
						title : '借阅时间'
					},{
						field : 'borstuts',
						title : '借阅状态 ',
						width : 150,
						formatter : function(value, row, index) {
							if (value == 0) {
								return '<font color="red">待借出</font>';
							} else if (value == 1) {
								return '<font color="blue">已借出</font>';
							} else if (value == 2) {
								return '<font color="green">已归还</font>';
							}
							return value;
						}
					},{
						field : 'num',
						width : 80,
						title : '借阅数量'
					},{
						field : 'gobacktime',
						width : 100,
						title : '归还时间'
					}]
		});
	};
	// button事件绑定
	this.buttonBind = function() {
		// 查询
		$("#btn_query").click(function() {
					$("#" + t.tableId).bootstrapTable('refresh', {
								pageNumber : 1
							});
				});

	}
	this.refreshTableData = function() {
		$("#" + t.tableId).bootstrapTable('refresh');
	}
	this.init = function() {
		t.tableBind();
		t.buttonBind();
	}
}
