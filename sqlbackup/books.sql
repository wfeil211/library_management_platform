/*
 Navicat Premium Data Transfer

 Source Server         : 百度云
 Source Server Type    : MySQL
 Source Server Version : 50562
 Source Host           : 121.36.192.170:3306
 Source Schema         : books

 Target Server Type    : MySQL
 Target Server Version : 50562
 File Encoding         : 65001

 Date: 21/08/2020 11:00:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `id` int(5) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `account` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '账号',
  `password` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '密码',
  `identity` int(1) DEFAULT NULL COMMENT '身份 0-用户 1-管理员',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, '123', '123', 1, '2019-04-16 13:27:54');
INSERT INTO `admin` VALUES (6, '123456', '123456', 0, '2019-04-18 14:21:46');
INSERT INTO `admin` VALUES (10, '111', '111', 0, '2019-05-11 20:23:15');

-- ----------------------------
-- Table structure for books
-- ----------------------------
DROP TABLE IF EXISTS `books`;
CREATE TABLE `books`  (
  `id` int(5) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '书名',
  `isbn` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '图书编号',
  `num` int(5) DEFAULT NULL COMMENT '数量',
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '封面',
  `info` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '详细信息',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `classify` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '分类',
  `author` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作者',
  `multimedia` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '多媒体',
  `video` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '视频',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of books
-- ----------------------------
INSERT INTO `books` VALUES (6, '大耳朵图图', '1234567896541', 20, '/BooksManagement/statics/upload/books/1597978141693936.jpg', '<p>大耳朵图图</p>', '2019-04-17 11:40:50', '历史', '佩奇', '/BooksManagement/statics/upload/multimedia/1597978373641.zip', '/BooksManagement/statics/upload/video/1597978756844.mp4');
INSERT INTO `books` VALUES (9, '大头儿子和小头爸爸', '1236547844561', 10, '/BooksManagement/statics/upload/books/1597978141693936.jpg', '<p>大头儿子和小头爸爸</p>', '2019-04-17 12:04:13', '历史', '佩奇', '/BooksManagement/statics/upload/multimedia/1597978373641.zip', '/BooksManagement/statics/upload/video/1597978756844.mp4');
INSERT INTO `books` VALUES (24, '海绵宝宝', '1236547896541', 10, '/BooksManagement/statics/upload/books/1597978141693936.jpg', '<p>海绵宝宝</p>', '2019-04-18 14:47:23', '历史', '佩奇', '/BooksManagement/statics/upload/multimedia/1597978373641.zip', '/BooksManagement/statics/upload/video/1597978756844.mp4');
INSERT INTO `books` VALUES (27, '天线宝宝', '1254698745632', 10, '/BooksManagement/statics/upload/books/1597978141693936.jpg', '<p>天线宝宝</p>', '2019-04-18 15:12:50', '历史', '佩奇', '/BooksManagement/statics/upload/multimedia/1597978373641.zip', '/BooksManagement/statics/upload/video/1597978756844.mp4');
INSERT INTO `books` VALUES (28, '熊出没', '2154698745632', 10, '/BooksManagement/statics/upload/books/1597978141693936.jpg', '<p>熊出没</p>', '2019-04-18 16:04:07', '历史', '佩奇', '/BooksManagement/statics/upload/multimedia/1597978373641.zip', '/BooksManagement/statics/upload/video/1597978756844.mp4');
INSERT INTO `books` VALUES (29, '爆笑虫子', '2546321587965', 20, '/BooksManagement/statics/upload/books/1597978141693936.jpg', '<p>爆笑虫子</p>', '2019-04-18 16:04:29', '历史', '佩奇', '/BooksManagement/statics/upload/multimedia/1597978373641.zip', '/BooksManagement/statics/upload/video/1597978756844.mp4');
INSERT INTO `books` VALUES (30, '奥特曼', '2365214587459', 30, '/BooksManagement/statics/upload/books/1597978141693936.jpg', '<p>奥特曼</p>', '2019-04-18 16:04:50', '历史', '佩奇', '/BooksManagement/statics/upload/multimedia/1597978373641.zip', '/BooksManagement/statics/upload/video/1597978756844.mp4');
INSERT INTO `books` VALUES (31, '葫芦兄弟', '3202152365489', 20, '/BooksManagement/statics/upload/books/1597978141693936.jpg', '<p>葫芦兄弟</p>', '2019-04-18 16:05:15', '历史', '佩奇', '/BooksManagement/statics/upload/multimedia/1597978373641.zip', '/BooksManagement/statics/upload/video/1597978756844.mp4');
INSERT INTO `books` VALUES (32, '成龙历险记', '2154698745635', 10, '/BooksManagement/statics/upload/books/1597978141693936.jpg', '<p>成龙历险记</p>', '2019-04-18 16:05:52', '历史', '佩奇', '/BooksManagement/statics/upload/multimedia/1597978373641.zip', '/BooksManagement/statics/upload/video/1597978756844.mp4');
INSERT INTO `books` VALUES (35, '小猪佩奇', '1254875965423', 10, '/BooksManagement/statics/upload/books/1597978141693936.jpg', '<p>小猪佩奇</p>', '2019-04-18 17:56:18', '历史', '佩奇', '/BooksManagement/statics/upload/multimedia/1597978373641.zip', '/BooksManagement/statics/upload/video/1597978756844.mp4');

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `id` int(5) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `orderno` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单号',
  `adminid` int(5) DEFAULT NULL COMMENT '账户id',
  `name` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '姓名',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '联系方式',
  `createtime` datetime DEFAULT NULL COMMENT '借阅时间',
  `num` int(5) DEFAULT NULL COMMENT '借阅数量',
  `gobacktime` datetime DEFAULT NULL COMMENT '归还时间',
  `isbn` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '图书编号',
  `borstuts` int(1) DEFAULT NULL COMMENT '借阅状态 0-待借出 1-已借出 2-已归还',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `adminid`(`adminid`) USING BTREE,
  INDEX `booksid`(`isbn`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of order
-- ----------------------------
INSERT INTO `order` VALUES (3, 'd54c88f22bf24527aed6f9ba160ad7e6', 1, '大头儿子', '18709282143', '2019-04-17 17:20:23', 10, '2019-04-18 00:00:00', '1236547844561', 2);
INSERT INTO `order` VALUES (5, 'cddda38c0093471b9a566292fd28c95c', 1, '大头儿子', '18709282143', '2019-04-17 17:33:04', 10, '2019-04-18 00:00:00', '1236547844561', 2);
INSERT INTO `order` VALUES (6, '502ea01cd16a41509058c22c0099abc8', 1, '大耳朵图图', '18709282143', '2019-04-18 10:30:20', 20, '2019-04-20 00:00:00', '1234567896541', 2);
INSERT INTO `order` VALUES (7, 'c226ca21f5a946f09d9af1f89ac53c6f', 1, '大耳朵图图', '18709282143', '2019-04-18 10:40:22', 10, '2019-04-19 00:00:00', '1234567896541', 2);
INSERT INTO `order` VALUES (8, 'f051a80380764d5c82c6923f7dffd3e1', 3, '图图', '18709282143', '2019-04-18 10:53:45', 10, '2019-04-19 00:00:00', '1234567896541', 2);
INSERT INTO `order` VALUES (9, '7b8479c5ab78409f93e524a5b841a408', 1, '图图', '18709282143', '2019-04-18 16:58:55', 10, '2019-04-19 00:00:00', '1234567896541', 2);
INSERT INTO `order` VALUES (10, 'eb8f412290cf450d802a445d3bfa7954', 6, '大头儿子', '18709282143', '2019-04-18 17:02:08', 10, '2019-04-19 00:00:00', '1236547844561', 0);

SET FOREIGN_KEY_CHECKS = 1;
