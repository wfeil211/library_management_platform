package linfei.util;

/**
 * ResultMsg工具类
 * @author 林飞
 * @author wfeil211@foxmail.com
 */
public class ResultMsg {
	private boolean success;
	private String msg;
	private Object data;
	private boolean execute;
	private int code;

	public ResultMsg() {
		super();
		this.success = true;
		this.execute = false;
	}
	
	public ResultMsg(boolean success, String msg, Object data, boolean execute){
		this.success = success;
		this.msg = msg;
		this.data = data;
		this.execute = execute;
	}

	public boolean isExecute() {
		return execute;
	}

	public void setExecute(boolean execute) {
		this.execute = execute;
	}

	public boolean getSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

}
