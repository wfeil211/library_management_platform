package linfei.util;

import java.util.List;
/**
 * ResultGridStore工具类
 * @author 林飞
 * @author wfeil211@foxmail.com
 */
public class ResultGridStore {
	private Integer totalProperty;
	private List<?> list;

	public Integer getTotalProperty() {
		return totalProperty;
	}

	public void setTotalProperty(Integer totalProperty) {
		this.totalProperty = totalProperty;
	}

	public List<?> getList() {
		return list;
	}

	public void setList(List<?> list) {
		this.list = list;
	}

}
