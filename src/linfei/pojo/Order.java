package linfei.pojo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 订单-实体
 * 
 * @author 林飞
 * @author wfeil211@foxmail.com
 */
public class Order {
    /**主键id*/
    private Integer id;

    /**订单号*/
    private String orderno;

    /**账户id*/
    private Integer adminid;

    /**姓名*/
    private String name;

    /**联系方式*/
    private String phone;

    /**借阅时间*/
    private Date createtime;

    /**借阅数量*/
    private Integer num;

    /**归还时间*/
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date gobacktime;

    /**图书编号*/
    private String isbn;

    /**借阅状态 0-待借出 1-已借出 2-已归还*/
    private Integer borstuts;

    /** get 主键id*/
    public Integer getId() {
        return id;
    }

    /** set 主键id*/
    public void setId(Integer id) {
        this.id = id;
    }

    /** get 订单号*/
    public String getOrderno() {
        return orderno;
    }

    /** set 订单号*/
    public void setOrderno(String orderno) {
        this.orderno = orderno == null ? null : orderno.trim();
    }

    /** get 账户id*/
    public Integer getAdminid() {
        return adminid;
    }

    /** set 账户id*/
    public void setAdminid(Integer adminid) {
        this.adminid = adminid;
    }

    /** get 姓名*/
    public String getName() {
        return name;
    }

    /** set 姓名*/
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /** get 联系方式*/
    public String getPhone() {
        return phone;
    }

    /** set 联系方式*/
    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    /** get 借阅时间*/
    public Date getCreatetime() {
        return createtime;
    }

    /** set 借阅时间*/
    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    /** get 借阅数量*/
    public Integer getNum() {
        return num;
    }

    /** set 借阅数量*/
    public void setNum(Integer num) {
        this.num = num;
    }

    /** get 归还时间*/
    public Date getGobacktime() {
        return gobacktime;
    }

    /** set 归还时间*/
    public void setGobacktime(Date gobacktime) {
        this.gobacktime = gobacktime;
    }

    /** get 图书编号*/
    public String getIsbn() {
        return isbn;
    }

    /** set 图书编号*/
    public void setIsbn(String isbn) {
        this.isbn = isbn == null ? null : isbn.trim();
    }

    /** get 借阅状态 0-待借出 1-已借出 2-已归还*/
    public Integer getBorstuts() {
        return borstuts;
    }

    /** set 借阅状态 0-待借出 1-已借出 2-已归还*/
    public void setBorstuts(Integer borstuts) {
        this.borstuts = borstuts;
    }
}