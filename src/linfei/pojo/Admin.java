package linfei.pojo;

import java.util.Date;

/**
 * 账号-实体
 * 
 * @author 林飞
 * @author wfeil211@foxmail.com
 */
public class Admin {
    /**主键id*/
    private Integer id;

    /**账号*/
    private String account;

    /**密码*/
    private String password;

    /**身份 0-用户 1-管理员*/
    private Integer identity;
    
    /**创建时间*/
    private Date createtime;

    public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Date getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	/** get 主键id*/
    public Integer getId() {
        return id;
    }

    /** set 主键id*/
    public void setId(Integer id) {
        this.id = id;
    }

    /** get 密码*/
    public String getPassword() {
        return password;
    }

    /** set 密码*/
    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    /** get 身份 0-用户 1-管理员*/
    public Integer getIdentity() {
        return identity;
    }

    /** set 身份 0-用户 1-管理员*/
    public void setIdentity(Integer identity) {
        this.identity = identity;
    }
}