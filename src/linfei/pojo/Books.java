package linfei.pojo;

import java.util.Date;

/**
 * 图书-实体
 * 
 * @author 林飞
 * @author wfeil211@foxmail.com
 */
public class Books {
    /**主键id*/
    private Integer id;

    /**书名*/
    private String name;

    /**图书编号*/
    private String isbn;

    /**数量*/
    private Integer num;

    /**封面*/
    private String image;

    /**详细信息*/
    private String info;

    /**创建时间*/
    private Date createtime;

    /**分类*/
    private String classify;

    /**作者*/
    private String author;
    
    /**多媒体*/
    private String multimedia;
    
    /**视频*/
    private String video;

    public String getVideo() {
		return video;
	}

	public void setVideo(String video) {
		this.video = video;
	}

	public String getMultimedia() {
		return multimedia;
	}

	public void setMultimedia(String multimedia) {
		this.multimedia = multimedia;
	}

	/** get 主键id*/
    public Integer getId() {
        return id;
    }

    /** set 主键id*/
    public void setId(Integer id) {
        this.id = id;
    }

    /** get 书名*/
    public String getName() {
        return name;
    }

    /** set 书名*/
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /** get 图书编号*/
    public String getIsbn() {
        return isbn;
    }

    /** set 图书编号*/
    public void setIsbn(String isbn) {
        this.isbn = isbn == null ? null : isbn.trim();
    }

    /** get 数量*/
    public Integer getNum() {
        return num;
    }

    /** set 数量*/
    public void setNum(Integer num) {
        this.num = num;
    }

    /** get 封面*/
    public String getImage() {
        return image;
    }

    /** set 封面*/
    public void setImage(String image) {
        this.image = image == null ? null : image.trim();
    }

    /** get 详细信息*/
    public String getInfo() {
        return info;
    }

    /** set 详细信息*/
    public void setInfo(String info) {
        this.info = info == null ? null : info.trim();
    }

    /** get 创建时间*/
    public Date getCreatetime() {
        return createtime;
    }

    /** set 创建时间*/
    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    /** get 分类*/
    public String getClassify() {
        return classify;
    }

    /** set 分类*/
    public void setClassify(String classify) {
        this.classify = classify == null ? null : classify.trim();
    }

    /** get 作者*/
    public String getAuthor() {
        return author;
    }

    /** set 作者*/
    public void setAuthor(String author) {
        this.author = author == null ? null : author.trim();
    }
}