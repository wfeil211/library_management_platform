package linfei.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import linfei.pojo.Order;
/**
 * 订单-接口
 * 
 * @author 林飞
 * @author wfeil211@foxmail.com
 */
public interface OrderMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table order
     *	根据主键删除
     * @mbggenerated Wed Apr 17 13:48:20 CST 2019
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table order
     *	添加
     * @mbggenerated Wed Apr 17 13:48:20 CST 2019
     */
    int insert(Order record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table order
     *	动态添加
     * @mbggenerated Wed Apr 17 13:48:20 CST 2019
     */
    int insertSelective(Order record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table order
     *	根据主键查询
     * @mbggenerated Wed Apr 17 13:48:20 CST 2019
     */
    Order selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table order
     *	根据主键动态更新
     * @mbggenerated Wed Apr 17 13:48:20 CST 2019
     */
    int updateByPrimaryKeySelective(Order record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table order
     *	根据主键更新
     * @mbggenerated Wed Apr 17 13:48:20 CST 2019
     */
    int updateByPrimaryKey(Order record);
    /**
     * 动态查询
     * @param build
     * @return List<Order>
     */
    List<Order> query(@Param("order")Order order,@Param("start")Integer start,@Param("limit")Integer limit);
    /**
     * 动态计数
     * @param build
     * @return int
     */
    int total(@Param("order")Order order);
}