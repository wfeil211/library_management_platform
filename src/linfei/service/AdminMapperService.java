package linfei.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import linfei.pojo.Admin;

/**
 * 账号-服务层接口
 * 
 * @author 林飞
 * @author wfeil211@foxmail.com
 */
public interface AdminMapperService {
	/**
     * 查询账号密码是否正确
     * @param record
     * @return Admin
     */
    Admin serviceSelectAdmin(Admin record);
    /**
     * 动态查询
     * @param build
     * @return List<Admin>
     */
    List<Admin> serviceQuery(Admin admin,Integer start,Integer limit);
    /**
     * 动态计数
     * @param build
     * @return int
     */
    int serviceTotal(Admin admin);
    /**
     * 添加
     * @param record
     * @return int
     */
    int serviceInsert(Admin record);
    /**
     * 批量删除
     * @param ids
     * @return int
     */
    int serviceBatchDelete(@Param("ids")String ids);
    /**
     * 根据主键更新
     * @param record
     * @return int
     */
    int serviceUpdateByPrimaryKeySelective(Admin record);
}
