package linfei.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import linfei.dao.OrderMapper;
import linfei.pojo.Order;
import linfei.service.OrderMapperService;
/**
 * 订单-服务层接口实现类
 * 
 * @author 林飞
 * @author wfeil211@foxmail.com
 */
@Service("orderMapperService")
public class OrderMapperServiceImpl implements OrderMapperService {

	@Autowired
	private OrderMapper orderMapper;
	@Override
	public List<Order> serviceQuery(Order order, Integer start, Integer limit) {
		// TODO Auto-generated method stub
		return orderMapper.query(order, start, limit);
	}

	@Override
	public int serviceTotal(Order order) {
		// TODO Auto-generated method stub
		return orderMapper.total(order);
	}

	@Override
	public int serviceInsert(Order record) {
		// TODO Auto-generated method stub
		return orderMapper.insert(record);
	}

	@Override
	public Order serviceSelectByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return orderMapper.selectByPrimaryKey(id);
	}

	@Override
	public int serviceUpdateByPrimaryKey(Order record) {
		// TODO Auto-generated method stub
		return orderMapper.updateByPrimaryKey(record);
	}

}
