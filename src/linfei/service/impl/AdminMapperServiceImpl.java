package linfei.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import linfei.dao.AdminMapper;
import linfei.pojo.Admin;
import linfei.service.AdminMapperService;
/**
 * 账号-服务层接口实现类
 * 
 * @author 林飞
 * @author wfeil211@foxmail.com
 */
@Service("adminMapperService")
public class AdminMapperServiceImpl implements AdminMapperService {

	@Autowired
	private AdminMapper adminMapper;
	@Override
	public Admin serviceSelectAdmin(Admin record) {
		// TODO Auto-generated method stub
		return adminMapper.selectAdmin(record);
	}
	@Override
	public List<Admin> serviceQuery(Admin admin, Integer start, Integer limit) {
		// TODO Auto-generated method stub
		return adminMapper.query(admin, start, limit);
	}
	@Override
	public int serviceTotal(Admin admin) {
		// TODO Auto-generated method stub
		return adminMapper.total(admin);
	}
	@Override
	public int serviceInsert(Admin record) {
		// TODO Auto-generated method stub
		return adminMapper.insert(record);
	}
	@Override
	public int serviceBatchDelete(String ids) {
		// TODO Auto-generated method stub
		return adminMapper.batchDelete(ids);
	}
	@Override
	public int serviceUpdateByPrimaryKeySelective(Admin record) {
		// TODO Auto-generated method stub
		return adminMapper.updateByPrimaryKeySelective(record);
	}

}
