package linfei.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import linfei.dao.BooksMapper;
import linfei.pojo.Books;
import linfei.service.BooksMapperService;
/**
 * 图书-服务层接口实现类
 * 
 * @author 林飞
 * @author wfeil211@foxmail.com
 */
@Service("booksMapperService")
public class BooksMapperServiceImpl implements BooksMapperService {

	@Autowired
	private BooksMapper booksMapper;
	@Override
	public List<Books> serviceQuery(Books books, Integer start, Integer limit) {
		// TODO Auto-generated method stub
		return booksMapper.query(books, start, limit);
	}

	@Override
	public int serviceTotal(Books books) {
		// TODO Auto-generated method stub
		return booksMapper.total(books);
	}

	@Override
	public int serviceInsert(Books books) {
		// TODO Auto-generated method stub
		return booksMapper.insert(books);
	}

	@Override
	public int serviceBatchDelete(String ids) {
		// TODO Auto-generated method stub
		return booksMapper.batchDelete(ids);
	}

	@Override
	public int serviceUpdateByPrimaryKey(Books record) {
		// TODO Auto-generated method stub
		return booksMapper.updateByPrimaryKey(record);
	}

	@Override
	public int serviceUpdateByPrimaryKeySelective(Books record) {
		// TODO Auto-generated method stub
		return booksMapper.updateByPrimaryKeySelective(record);
	}

}
