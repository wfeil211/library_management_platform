package linfei.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import linfei.pojo.Books;

/**
 * 图书-服务层接口
 * 
 * @author 林飞
 * @author wfeil211@foxmail.com
 */
public interface BooksMapperService {
	/**
     * 动态查询
     * @param build
     * @return List<Books>
     */
    List<Books> serviceQuery(Books books,Integer start,Integer limit);
    /**
     * 动态计数
     * @param build
     * @return int
     */
    int serviceTotal(Books books);
    /**
     * 添加
     * @param record
     * @return int
     */
    int serviceInsert(Books books);
    /**
     * 批量删除
     * @param ids
     * @return int
     */
    int serviceBatchDelete(@Param("ids")String ids);
    /**
     * 更新
     * @param record
     * @return int
     */
    int serviceUpdateByPrimaryKey(Books record);
    /**
     * 动态更新
     * @param record
     * @return int
     */
    int serviceUpdateByPrimaryKeySelective(Books record);
}
