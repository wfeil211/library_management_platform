package linfei.service;

import java.util.List;

import linfei.pojo.Order;

/**
 * 订单-服务层接口
 * 
 * @author 林飞
 * @author wfeil211@foxmail.com
 */
public interface OrderMapperService {
	/**
     * 动态查询
     * @param build
     * @return List<Order>
     */
    List<Order> serviceQuery(Order order,Integer start,Integer limit);
    /**
     * 动态计数
     * @param build
     * @return int
     */
    int serviceTotal(Order order);
    /**
     * 添加
     * @param record
     * @return int
     */
    int serviceInsert(Order record);
    /**
     * 根据主键查询
     * @param id
     * @return Order
     */
    Order serviceSelectByPrimaryKey(Integer id);
    /**
     * 根据主键更新
     * @param record
     * @return int
     */
    int serviceUpdateByPrimaryKey(Order record);
}
