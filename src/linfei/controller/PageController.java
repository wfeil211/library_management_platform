package linfei.controller;

import java.io.Serializable;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * 页面跳转-控制层
 * 
 * @author 林飞
 * @author wfeil211@foxmail.com
 */
@Controller
@RequestMapping("PageController")
public class PageController implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@RequestMapping("/requestPage")
	public String requestPage(String page){
		return page;
	}

}
