package linfei.controller;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import linfei.util.ResultGridStore;
import linfei.util.ResultMsg;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import linfei.pojo.Admin;
import linfei.service.AdminMapperService;
/**
 * 账号-控制层
 * 
 * @author 林飞
 * @author wfeil211@foxmail.com
 */
@Controller
@RequestMapping("UserController")
public class UserController implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(UserController.class);
	@Autowired
	private AdminMapperService adminMapperService;
	/**
	 * 登录
	 * @param admin
	 * @param model
	 * @return String
	 */
	@RequestMapping("/login")
	public String login(Admin admin,Model model,HttpServletRequest req,HttpServletResponse resp){
		try{
			Admin adm=adminMapperService.serviceSelectAdmin(admin);
			if(adm!=null){
				model.addAttribute("name", adm.getAccount());
				req.getSession().setAttribute("adm", adm);
				//创建Cookies
				Cookie cookie=new Cookie("identity",adm.getIdentity()+"");
				cookie.setPath("/");
				resp.addCookie(cookie);
				if(adm.getIdentity()==0){
					return "login/indexUser";
				}else if(adm.getIdentity()==1){
					return "login/index";
				}				
			}else{
				model.addAttribute("error", "账号密码不正确");
				return "forward:/login.jsp";
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("系统出错!", e);
		}
		return "login/index";
	}
	/**
	 * 表单获取
	 * @param admin
	 * @param start
	 * @param limit
	 * @return ResultGridStore
	 */
	@RequestMapping("/getGridStore")
	@ResponseBody
	public ResultGridStore getGridStore(Admin admin,Integer start,Integer limit){
		ResultGridStore rgs=new ResultGridStore();
		try{
			rgs.setList(adminMapperService.serviceQuery(admin, start, limit));
			rgs.setTotalProperty(adminMapperService.serviceTotal(admin));
		}catch(Exception e){
			e.printStackTrace();
			logger.error("系统出错!", e);
		}
		return rgs;
	}
	/**
	 * 添加用户
	 * @param admin
	 * @return ResultMsg
	 */
	@RequestMapping("/addUser")
	@ResponseBody
	public ResultMsg addUser(Admin admin){
		ResultMsg rsg=new ResultMsg();
		try{
			Admin account=new Admin();
			account.setAccount(admin.getAccount());
			List<Admin> ad=adminMapperService.serviceQuery(account, 0, 10);
			if(ad.size()>0){
				rsg.setExecute(false);
				rsg.setSuccess(false);
				rsg.setMsg("该账号已存在，请重试!");
				return rsg;
			}
			admin.setCreatetime(new Date());
			adminMapperService.serviceInsert(admin);
			rsg.setExecute(true);
			rsg.setSuccess(true);
			rsg.setMsg("添加成功!");
		}catch(Exception e){
			rsg.setExecute(false);
			rsg.setSuccess(false);
			rsg.setMsg("系统出错!");
			e.printStackTrace();
			logger.error("系统出错!", e);
		}
		return rsg;
	}
	/**
	 * 修改用户
	 * @param admin
	 * @return ResultMsg
	 */
	@RequestMapping("/editUser")
	@ResponseBody
	public ResultMsg editUser(Admin admin){
		ResultMsg rsg=new ResultMsg();
		try{
			adminMapperService.serviceUpdateByPrimaryKeySelective(admin);
			rsg.setExecute(true);
			rsg.setSuccess(true);
			rsg.setMsg("修改成功!");
		}catch(Exception e){
			rsg.setExecute(false);
			rsg.setSuccess(false);
			rsg.setMsg("系统出错!");
			e.printStackTrace();
			logger.error("系统出错!", e);
		}
		return rsg;
	}
	/**
	 * 批量删除
	 * @param ids
	 * @return ResultMsg
	 */
	@RequestMapping("/delUser")
	@ResponseBody
	public ResultMsg delUser(String ids){
		ResultMsg msg = new ResultMsg();
		try {
			if (!(StringUtils.isEmpty(ids))) {
					int row = adminMapperService.serviceBatchDelete(ids);
					if (row > 0) {
						msg.setExecute(true);
						msg.setSuccess(true);
						msg.setMsg("删除成功!");
					} else {
						msg.setExecute(false);
						msg.setSuccess(false);
						msg.setMsg("删除失败!");
					}
			} else {
				msg.setExecute(false);
				msg.setSuccess(false);
				msg.setMsg("数据传输失败!");
			}
		} catch (Exception e) {
			msg.setExecute(false);
			msg.setSuccess(false);
			msg.setMsg("系统出错!");
			e.printStackTrace();
			logger.error("系统出错!", e);
		}
		return msg;
	}
	/**
	 * 注册用户
	 * @param admin
	 * @return ResultMsg
	 */
	@RequestMapping("/register")
	public String register(Admin admin,Model model){
		try{
			Admin account=new Admin();
			account.setAccount(admin.getAccount());
			List<Admin> ad=adminMapperService.serviceQuery(account, 0, 10);
			if(ad.size()>0){
				//账号存在
				model.addAttribute("register", "该账号已经存在，请重试");
				return "register/register";
			}
			model.addAttribute("register", "注册成功！");
			admin.setCreatetime(new Date());
			admin.setIdentity(0);
			adminMapperService.serviceInsert(admin);
		}catch(Exception e){
			model.addAttribute("register", "系统出错!");
			e.printStackTrace();
			logger.error("系统出错!", e);
		}
		return "register/register";
	}
}
